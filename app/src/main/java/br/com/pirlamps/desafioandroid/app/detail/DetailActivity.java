package br.com.pirlamps.desafioandroid.app.detail;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.pirlamps.desafioandroid.R;
import br.com.pirlamps.desafioandroid.app.source.SourceActivity;
import br.com.pirlamps.desafioandroid.databinding.ActivityDetailBinding;
import br.com.pirlamps.desafioandroid.foundation.application.DesafioAndroidApplication;
import br.com.pirlamps.desafioandroid.foundation.custom.EndlessRecyclerViewScrollListener;
import br.com.pirlamps.desafioandroid.foundation.custom.joat.JoatObject;
import br.com.pirlamps.desafioandroid.foundation.custom.joat.JoatRecyclerAdapter;
import br.com.pirlamps.desafioandroid.foundation.model.GitPullRequest;
import br.com.pirlamps.desafioandroid.foundation.model.Item;
import br.com.pirlamps.desafioandroid.foundation.model.SpacesItemDecoration;

/*
 * Created by root-matheus on 21/04/17.
 */

public class DetailActivity extends AppCompatActivity implements DetailContract.View, JoatRecyclerAdapter.JoatRecyclerDelegate {

    private final String TAG = DetailActivity.class.getName();

    @Inject
    DetailPresenter mPresenter;

    private ActivityDetailBinding mBinding;
    private LinearLayoutManager mLayoutManager;
    private JoatRecyclerAdapter mAdapter;
    private EndlessRecyclerViewScrollListener mScrollListener;

    private int mTotalItensCount;

    private Item mRepo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail);

        DaggerDetailComponent.builder()
                .netComponent(((DesafioAndroidApplication) getApplicationContext()).getmNetComponent())
                .detailModule(new DetailModule(this))
                .build()
                .inject(this);

        mRepo = (Item) getIntent().getSerializableExtra(Item.ITEM_TAG);

        getSupportActionBar().setTitle(mRepo.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.i(TAG, "onCreate: settingUp view");
        mLayoutManager = new LinearLayoutManager(this);
        SpacesItemDecoration space = new SpacesItemDecoration(20);
        mAdapter = new JoatRecyclerAdapter(R.layout.row_detail);
        mAdapter.setmDelegate(this);

        mScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mTotalItensCount = totalItemsCount;
                mPresenter.loadPullRequests(page,mRepo);

            }
        };

        mBinding.outletDetailList.setHasFixedSize(true);
        mBinding.outletDetailList.setLayoutManager(mLayoutManager);
        mBinding.outletDetailList.addItemDecoration(space);
        mBinding.outletDetailList.setAdapter(mAdapter);
        mBinding.outletDetailList.addOnScrollListener(mScrollListener);

        mPresenter.loadPullRequests(1,mRepo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                finish();
                Log.i(TAG, "onOptionsItemSelected: pressed Home Button");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showPullRequests(List<GitPullRequest> pullRequest) {
        List<JoatObject> dataSource = new ArrayList<>();

        Log.i(TAG, "showPullRequests: assembling JoatList for adapter");
        for (GitPullRequest pull: pullRequest ) {
            dataSource.add(new JoatObject(BR.gitPull, pull));
        }

        if(dataSource.isEmpty()){
            mBinding.outletEmptyLayout.setVisibility(View.VISIBLE);
        }
        mAdapter.addData(mTotalItensCount,dataSource);
    }

    @Override
    public void showError(String message) {

        Log.i(TAG, "showError: preparing to  show error snackbar");
        Snackbar snackbar = Snackbar
                .make(mBinding.getRoot(), message, Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction(R.string.snack_action_reload, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.loadPullRequests(mScrollListener.getCurrentPage(), mRepo);
            }
        });
        snackbar.show();
    }

    @Override
    public void showComplete() {
        Log.i(TAG, "showComplete: service call completed");
    }

    @Override
    public void didTouchItem(int position) {

        Log.i(TAG, "didTouchItem: at position"+position);
        GitPullRequest pull = mAdapter.getItemWithType(position, GitPullRequest.class);
        Intent intent = new Intent(DetailActivity.this, SourceActivity.class);
        intent.putExtra(GitPullRequest.PULL_TAG, pull);
        startActivity(intent);
    }
}
