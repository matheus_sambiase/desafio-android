package br.com.pirlamps.desafioandroid.app.source;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import br.com.pirlamps.desafioandroid.R;
import br.com.pirlamps.desafioandroid.databinding.ActivitySourceBinding;
import br.com.pirlamps.desafioandroid.foundation.model.GitPullRequest;

/**
 * Created by root-matheus on 22/04/17.
 */

public class SourceActivity extends AppCompatActivity {

    private final String TAG = SourceActivity.class.getName();
    private ActivitySourceBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_source);

        getSupportActionBar().setTitle("Desafio Android");

        GitPullRequest pull = ((GitPullRequest) getIntent().getSerializableExtra(GitPullRequest.PULL_TAG));
        getSupportActionBar().setTitle(pull.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.i(TAG, "onCreate: preparing to load url:"+pull.getHtmlUrl()+" at webview");
        mBinding.outletWebView.loadUrl(pull.getHtmlUrl());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                finish();
                Log.i(TAG, "onOptionsItemSelected: pressed Home Button");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
