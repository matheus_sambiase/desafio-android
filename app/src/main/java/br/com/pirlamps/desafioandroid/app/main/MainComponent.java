package br.com.pirlamps.desafioandroid.app.main;

import br.com.pirlamps.desafioandroid.foundation.component.NetComponent;
import br.com.pirlamps.desafioandroid.foundation.custom.CustomScope;
import dagger.Component;

/*
 * Created by root-matheus on 21/04/17.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = MainModule.class)
interface MainComponent {
    void inject(MainActivity activity);
}