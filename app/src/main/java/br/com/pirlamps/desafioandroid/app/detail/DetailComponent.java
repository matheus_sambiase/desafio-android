package br.com.pirlamps.desafioandroid.app.detail;

import br.com.pirlamps.desafioandroid.app.main.MainModule;
import br.com.pirlamps.desafioandroid.foundation.component.NetComponent;
import br.com.pirlamps.desafioandroid.foundation.custom.CustomScope;
import dagger.Component;

/**
 * Created by root-matheus on 21/04/17.
 */
@CustomScope
@Component(dependencies = NetComponent.class, modules = DetailModule.class)
public interface DetailComponent {
    void inject(DetailActivity activity);
}
