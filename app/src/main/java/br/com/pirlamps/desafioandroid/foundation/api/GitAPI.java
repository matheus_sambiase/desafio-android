package br.com.pirlamps.desafioandroid.foundation.api;

import java.util.List;

import br.com.pirlamps.desafioandroid.foundation.model.GitPullRequest;
import br.com.pirlamps.desafioandroid.foundation.model.GitRepoRequest;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by root-matheus on 21/04/17.
 */

public interface GitAPI {

    @GET("/search/repositories")
    Observable<GitRepoRequest> getRepos(
            @Query("q") String q,
            @Query("sort") String sort,
            @Query("page") int page,
            @Query("per_page") int per_page
    );

    @GET("/repos/{login}/{repo_name}/pulls")
    Observable<List<GitPullRequest>> getPullRequests(
            @Path("login") String login,
            @Path("repo_name") String repoName,
            @Query("sort") String sort,
            @Query("page") int page,
            @Query("per_page") int per_page
    );

}
