package br.com.pirlamps.desafioandroid.app.detail;

import java.util.List;

import br.com.pirlamps.desafioandroid.foundation.model.GitPullRequest;
import br.com.pirlamps.desafioandroid.foundation.model.GitRepoRequest;
import br.com.pirlamps.desafioandroid.foundation.model.Item;

/**
 * Created by root-matheus on 21/04/17.
 */

public interface DetailContract {

    interface View{
        void showPullRequests(List<GitPullRequest> pullRequest);

        void showError(String message);

        void showComplete();
    }

    interface Presenter{

        void loadPullRequests(int page, Item repo);

    }

}
