package br.com.pirlamps.desafioandroid.app.main;

import br.com.pirlamps.desafioandroid.foundation.model.GitRepoRequest;

/*
 * Created by root-matheus on 21/04/17.
 */

public interface MainContract  {

    interface View{
        void showRepos(GitRepoRequest repoRequest);

        void showError(String message);

        void showComplete();
    }

    interface Presenter{

        void laodRepos(int page);

    }
}
