package br.com.pirlamps.desafioandroid.app.main;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;


import com.android.databinding.library.baseAdapters.BR;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.pirlamps.desafioandroid.R;
import br.com.pirlamps.desafioandroid.app.detail.DetailActivity;
import br.com.pirlamps.desafioandroid.databinding.ActivityMainBinding;
import br.com.pirlamps.desafioandroid.foundation.application.DesafioAndroidApplication;
import br.com.pirlamps.desafioandroid.foundation.custom.EndlessRecyclerViewScrollListener;
import br.com.pirlamps.desafioandroid.foundation.custom.joat.JoatObject;
import br.com.pirlamps.desafioandroid.foundation.custom.joat.JoatRecyclerAdapter;
import br.com.pirlamps.desafioandroid.foundation.model.GitRepoRequest;
import br.com.pirlamps.desafioandroid.foundation.model.Item;
import br.com.pirlamps.desafioandroid.foundation.model.SpacesItemDecoration;

/*
 * Created by root-matheus on 21/04/17.
 */

public class MainActivity extends AppCompatActivity implements MainContract.View, JoatRecyclerAdapter.JoatRecyclerDelegate{

    private final String TAG = MainActivity.class.getName();

    @Inject
    MainPresenter mPresenter;

    private ActivityMainBinding mBinding;
    private LinearLayoutManager mLayoutManager;
    private JoatRecyclerAdapter mAdapter;
    private EndlessRecyclerViewScrollListener mScrollListener;

    private int mTotalItensCount;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setando Layout
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        //Inicializando  Dagger
        DaggerMainComponent.builder()
                .netComponent(((DesafioAndroidApplication) getApplicationContext()).getmNetComponent())
                .mainModule(new MainModule(this))
                .build()
                .inject(this);

        getSupportActionBar().setTitle("Desafio Android");

        Log.i(TAG, "onCreate: settingUp view");
        //Inicializando componentes para lista
        mLayoutManager = new LinearLayoutManager(this);
        SpacesItemDecoration space = new SpacesItemDecoration(20);
        mAdapter = new JoatRecyclerAdapter(R.layout.row_main);
        mAdapter.setmDelegate(this);
        mScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mTotalItensCount = totalItemsCount;
                mPresenter.laodRepos(page);

            }
        };

        //Setando MainList
        mBinding.outletMainList.setHasFixedSize(true);
        mBinding.outletMainList.setLayoutManager(mLayoutManager);
        mBinding.outletMainList.addItemDecoration(space);
        mBinding.outletMainList.setAdapter(mAdapter);
        mBinding.outletMainList.addOnScrollListener(mScrollListener);


        mPresenter.laodRepos(1);
    }

    @Override
    public void showRepos(GitRepoRequest repoRequest) {

        List<JoatObject> dataSource = new ArrayList<>();

        Log.i(TAG, "showPullRequests: assembling JoatList for adapter");
        for (Item item: repoRequest.getItems() ) {
            dataSource.add(new JoatObject(BR.gitItem, item));
        }

        mAdapter.addData(mTotalItensCount,dataSource);

    }

    @Override
    public void showError(String message) {

        Log.i(TAG, "showError: preparing to  show error snackbar");
        Snackbar snackbar = Snackbar
                .make(mBinding.getRoot(), message, Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction(R.string.snack_action_reload, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.laodRepos(mScrollListener.getCurrentPage());
            }
        });
        snackbar.show();





    }

    @Override
    public void showComplete() {
        Log.i(TAG, "showComplete: service call completed");
    }

    @Override
    public void didTouchItem(int position) {
        Log.i(TAG, "didTouchItem: at position"+position);
        Item repo = mAdapter.getItemWithType(position, Item.class);
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra(Item.ITEM_TAG, repo);
        startActivity(intent);
    }
}
