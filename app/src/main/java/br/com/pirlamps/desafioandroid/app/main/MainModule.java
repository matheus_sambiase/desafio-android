package br.com.pirlamps.desafioandroid.app.main;

import br.com.pirlamps.desafioandroid.foundation.custom.CustomScope;
import dagger.Module;
import dagger.Provides;

/*
 * Created by root-matheus on 21/04/17.
 */

@Module
public class MainModule {

    private final MainContract.View mView;

    public MainModule(MainContract.View mView){
        this.mView = mView;
    }

    @Provides
    @CustomScope
    MainContract.View provideConceptContractView(){
        return this.mView;
    }

}
