package br.com.pirlamps.desafioandroid.app.detail;

import br.com.pirlamps.desafioandroid.foundation.custom.CustomScope;
import dagger.Module;
import dagger.Provides;

/**
 * Created by root-matheus on 21/04/17.
 */

@Module
public class DetailModule {

    private final DetailContract.View mView;

    public DetailModule(DetailContract.View mView){
        this.mView = mView;
    }

    @Provides
    @CustomScope
    DetailContract.View provideConceptContractView(){
        return this.mView;
    }
}
