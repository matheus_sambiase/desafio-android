package br.com.pirlamps.desafioandroid.app.detail;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import br.com.pirlamps.desafioandroid.foundation.api.GitAPI;
import br.com.pirlamps.desafioandroid.foundation.model.GitPullRequest;
import br.com.pirlamps.desafioandroid.foundation.model.Item;
import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by root-matheus on 21/04/17.
 */

public class DetailPresenter implements DetailContract.Presenter {

    private final String TAG = DetailPresenter.class.getName();

    private Retrofit mRetrofit;
    private DetailContract.View mView;

    @Inject
    public DetailPresenter(Retrofit mRetrofit, DetailContract.View mView) {
        this.mRetrofit = mRetrofit;
        this.mView = mView;
    }


    @Override
    public void loadPullRequests(final int page, Item repo) {

        Log.i(TAG, "laodPullRequests: requesting page: "+page);
        mRetrofit.create(GitAPI.class).getPullRequests(repo.getOwner().getLogin(),repo.getName(),"stars",page,30)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<List<GitPullRequest>>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                        Log.i(TAG, "onCompleted request");
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e.getMessage());
                        Log.e(TAG, "onError requesting page"+page,e);
                    }

                    @Override
                    public void onNext(List<GitPullRequest> pullRequest) {
                        mView.showPullRequests(pullRequest);
                        Log.i(TAG, "success parsing object");
                    }
                });
    }
}
