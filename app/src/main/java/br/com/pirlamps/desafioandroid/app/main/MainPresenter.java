package br.com.pirlamps.desafioandroid.app.main;



import android.util.Log;

import javax.inject.Inject;

import br.com.pirlamps.desafioandroid.foundation.api.GitAPI;
import br.com.pirlamps.desafioandroid.foundation.model.GitRepoRequest;
import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/*
 * Created by root-matheus on 21/04/17.
 */

public class MainPresenter implements MainContract.Presenter {

    private final String TAG = MainPresenter.class.getName();

    private Retrofit mRetrofit;
    private MainContract.View mView;

    @Inject
    public MainPresenter(Retrofit retrofit, MainContract.View mView){
        this.mRetrofit = retrofit;
        this.mView = mView;


    }

    @Override
    public void laodRepos(final int page) {

        Log.i(TAG, "laodRepos: requesting page: "+page);
        mRetrofit.create(GitAPI.class).getRepos("language:Java","stars",page,30)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<GitRepoRequest>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                        Log.i(TAG, "onCompleted request");
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e.getMessage());
                        Log.e(TAG, "onError requesting page"+page,e);
                    }

                    @Override
                    public void onNext(GitRepoRequest repoRequest) {
                        mView.showRepos(repoRequest);
                        Log.i(TAG, "success parsing object");
                    }
                });

    }
}
